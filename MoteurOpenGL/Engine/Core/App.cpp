#include "App.h"

App::App() {

	
	glfwInit();

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(width, height, "Moteur OpenGL", NULL, NULL);

	if (window == nullptr) {

		fprintf(stderr, "Failed to open GLFW window.");
		glfwTerminate();
		return;

	}

	glfwMakeContextCurrent(window);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glewExperimental = true;
	glewInit();

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glViewport(0, 0, width, height);

	glfwSetFramebufferSizeCallback(window, App::framebuffer_size_callback);

}

void App::init() {

	camera = new Camera(glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f));

	shadow = new Shadow(width, height);
	framebuffer = new Framebuffer(width, height, "FramebufferMaterial");
	
	Mesh *sponza = new Mesh(new TextureMaterial(), "sponza.obj");
	sponza->initMesh();
	sponza->translate(glm::vec3(2.0f, 0.0f, 0.0f));
	sponza->addTexture("wall.jpg");
	sponza->scale(glm::vec3(0.02f));
	//meshs.push_back(sponza);

	Mesh *chalet = new Mesh(new TextureMaterial(), "chalet.obj");
	chalet->initMesh();
	chalet->translate(glm::vec3(-2.0f, 0.0f, 1.0f));
	chalet->addTexture("chalet.jpg");
	meshs.push_back(chalet);

	Mesh *cube = new Mesh(new TextureMaterial());
	cube->setForm("cube");
	cube->initMesh();
	cube->translate(glm::vec3(0.0f, 0.55f, 0.0f));
	cube->scale(glm::vec3(1.0f));
	cube->addTexture("water.png");
	meshs.push_back(cube);

	Mesh *plane = new Mesh(new PhongMaterial(), "plane100r.obj");
	plane->initMesh();
	plane->translate(glm::vec3(0.0f, -0.01f, 0.0f));
	plane->scale(glm::vec3(1.0f));
	meshs.push_back(plane);

	/* TRANSPARENCE NON FONCTIONNEL ?
	Mesh *grass = new Mesh(new PhongMaterial());
	grass->setForm("plane");
	grass->initMesh();
	grass->translate(glm::vec3(0.0f, 0.0f, 0.0f));
	grass->addTexture("grass.png");
	
	meshs.push_back(grass);
	*/

	Mesh *water = new Mesh(new WaterMaterial(), "plane100r.obj");
	water->setForm("plane");
	water->initMesh();
	water->addTexture("water.png");
	water->translate(glm::vec3(0.0f, -2.0f, 0.0f));
	water->scale(glm::vec3(0.5f));

	meshs.push_back(water);

	light = new Light();
	light->color = glm::vec3(0.0f, 1.0f, 0.0f);
	//light->pos = glm::vec3(0.0f, 1.0f, 0.0f);
	light->pos = glm::vec3(-2.0f, 4.0f, 1.0f);


}

void App::run() {

	while(glfwWindowShouldClose(window) == 0){

		processInput();

		glm::mat4 model;
		glm::mat4 view;
		glm::mat4 proj;

		float time = (float)glfwGetTime();

		model = glm::rotate(model, time * glm::radians(50.0f), glm::vec3(0.5f, 1.0f, 0.0f));
		proj = glm::perspective(glm::radians(45.0f), width/height , 0.1f, 1000.0f);

		glm::vec3 mPos = meshs[1]->getPos();

		shadow->configureLight(light, camera);

		//shadow pass
		glViewport(0, 0, width, height);
		shadow->bind();
		glClear(GL_DEPTH_BUFFER_BIT);

		//for resolve some shadow issues
		glCullFace(GL_FRONT);
		draw(proj, time, true);
		glCullFace(GL_BACK);

		shadow->release();
		
		//render pass into the framebuffer
		//framebuffer->bind();
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		draw(proj, time, false);
		
		//framebuffer->release();

		//draw all pass
		//framebuffer->render(time);

		glViewport(0, 0, 256, 256);
		shadow->render(time);

		glfwSwapBuffers(window);
		glfwPollEvents();

		//light->pos.x = cos(time);

	}

	glfwTerminate();

}

void App::draw(glm::mat4 proj, float time, bool shadowDraw) {

	for (Mesh *m : meshs) {

		if (shadowDraw) {
			
			shadow->material->use();

			shadow->material->setMat4("model", m->getModel());

			shadow->material->setMat4("lightSpaceMatrix", shadow->getLightSpaceMatrix());

		}
		else {

			m->getMaterial()->use();

			m->getMaterial()->setMat4("lightSpaceMatrix", shadow->getLightSpaceMatrix());
			
			m->bindTextures();

			m->getMaterial()->setMat4("proj", proj);
			m->getMaterial()->setMat4("view", camera->getMatrixView());

			m->getMaterial()->setVec3("lightPos", light->pos);
			m->getMaterial()->setVec3("lightColor", light->color);
			m->getMaterial()->setVec3("camera", camera->getPos());

			m->prepareMaterial(time);
			
			glActiveTexture(GL_TEXTURE1);
			shadow->bindTexture();
			m->getMaterial()->setInt("shadowMap", 1);
			
		}

		m->draw(time);

	}

}

void App::processInput() {

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
		camera->moveCamera(camera->getCameraFront());
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
		camera->moveCamera(-camera->getCameraFront());
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		camera->moveCamera(-glm::normalize(glm::cross(camera->getCameraFront(), camera->getCameraUp())));
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		camera->moveCamera(glm::normalize(glm::cross(camera->getCameraFront(), camera->getCameraUp())));
	}
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		camera->moveCamera(glm::vec3(0.0f, 1.0f, 0.0f));
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
		camera->moveCamera(glm::vec3(0.0f, -1.0f, 0.0f));
	}

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {

		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);

		camera->orienteCamera(xpos, ypos);

	}
	else if (!camera->isFirstOriente() && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE) {
		camera->setFirstOriente(true);
	}

}

void App::framebuffer_size_callback(GLFWwindow *window, int width, int height) {

	glViewport(0, 0, width, height);

}