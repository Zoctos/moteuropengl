#pragma once

#include <stdio.h>
#include <stdlib.h>

#include "Camera.h"
#include "Framebuffer.h"
#include "Light.h"
#include "Mesh.h"
#include "Shadow.h"

#include "../../Materials/DepthMaterial/DepthMaterial.h"
#include "../../Materials/PhongMaterial/PhongMaterial.h"
#include "../../Materials/TextureMaterial/TextureMaterial.h"
#include "../../Materials/WaterMaterial/WaterMaterial.h"

#define GLM_FORCE_RADIANS

class App {

public :
	App();
	
	void init();
	void run();

	void processInput();
	void draw(glm::mat4 proj, float time, bool shadowDraw);

	static void framebuffer_size_callback(GLFWwindow *window, int width, int height);

private:
	GLFWwindow *window;

	std::vector<Mesh*> meshs;

	float width = 1024.0f;
	float height = 768.0f;

	Light *light;
	Camera *camera;
	Framebuffer *framebuffer;
	Framebuffer *inversion;
	Shadow *shadow;

};