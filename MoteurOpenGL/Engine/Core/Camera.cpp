#include "Camera.h"

Camera::Camera(glm::vec3 pos, glm::vec3 target, glm::vec3 up, glm::vec3 front) {

	this->pos = pos;
	this->cameraTarget = target;
	this->cameraDirection = glm::normalize(pos - target);

	this->up = up;
	this->cameraRight = glm::normalize(glm::cross(up, cameraDirection));

	this->cameraUp = glm::cross(this->cameraDirection, this->cameraRight);

	this->cameraFront = front;

}

void Camera::moveCamera(glm::vec3 move) {

	this->pos += (move * cameraSpeed);

}

void Camera::orienteCamera(double xpos, double ypos) {

	if (firstOriente) {
		lastX = xpos;
		lastY = ypos;
		firstOriente = false;
	}

	float xOffset = xpos - lastX;
	float yOffset = lastY - ypos;
	lastX = xpos;
	lastY = ypos;

	float sensitivity = 0.05f;
	xOffset *= sensitivity;
	yOffset *= sensitivity;

	yaw += xOffset;
	pitch += yOffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;

	glm::vec3 f;
	f.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
	f.y = sin(glm::radians(pitch));
	f.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
	this->cameraFront = glm::normalize(f);

}

void Camera::setFirstOriente(bool o) {
	this->firstOriente = o;
}

glm::mat4 Camera::getMatrixView() {

	glm::mat4 view;
	view = glm::lookAt(this->pos, this->pos + this->cameraFront, this->cameraUp);

	return view;

}

glm::vec3 Camera::getPos() {
	return this->pos;
}

glm::vec3 Camera::getCameraFront() {
	return this->cameraFront;
}

glm::vec3 Camera::getCameraUp() {
	return this->cameraUp;
}

bool Camera::isFirstOriente() {
	return this->firstOriente;
}