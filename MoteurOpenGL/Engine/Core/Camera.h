#pragma once

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include <iostream>

class Camera {

public:
	Camera(glm::vec3 pos, glm::vec3 target, glm::vec3 up, glm::vec3 front);

	void moveCamera(glm::vec3 move);
	void orienteCamera(double xpos, double ypos);
	void setFirstOriente(bool o);

	glm::mat4 getMatrixView();
	glm::vec3 getPos();
	glm::vec3 getCameraFront();
	glm::vec3 getCameraUp();
	bool isFirstOriente();

private:
	glm::vec3 pos;
	glm::vec3 cameraTarget;
	glm::vec3 cameraDirection;
	glm::vec3 up;
	glm::vec3 cameraRight;
	glm::vec3 cameraUp;
	glm::vec3 cameraFront;

	glm::mat4 view;

	float cameraSpeed = 0.05f;

	bool firstOriente = true;
	float lastX = 400;
	float lastY = 300;
	float yaw = -80.0f;
	float pitch = 0.0f;


};