#include "Framebuffer.h"

Framebuffer::Framebuffer(int width, int height, std::string name) {

	//------------------ FRAMEBUFFER
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	//------------------ TEXTURE
	texture = new Texture(width, height);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->getID(), 0);

	//------------------- RENDERBUFFER
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);


	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "framebuffer error" << std::endl;
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	quad = new Mesh(name);
	quad->setForm("plane");
	quad->initMesh();

}

void Framebuffer::bind() {

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

}

void Framebuffer::release() {

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void Framebuffer::render(float time) {

	quad->getMaterial()->use();
	glDisable(GL_DEPTH_TEST);
	glBindTexture(GL_TEXTURE_2D, texture->getID());
	quad->draw(time);

}