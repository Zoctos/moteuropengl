#pragma once

#include <iostream>

#include "Mesh.h"
#include "Texture.h"

class Framebuffer {

public:
	Framebuffer(int width, int height, std::string name);

	void bind();
	void release();
	void render(float time);

protected:
	unsigned int fbo; //fbo
	unsigned int rbo; //renderobject buffer

	Texture *texture;
	Mesh *quad;

};