#pragma once

#include <glm\glm.hpp>

class Light {

public:
	glm::vec3 pos;
	glm::vec3 color;

};