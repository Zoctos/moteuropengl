#include "Material.h"

Material::Material(std::string name) {

	this->name = name;

	this->vertexCode = Tools::readFile("Materials/" + name + "/Shader/vertex.glsl");
	this->fragmentCode = Tools::readFile("Materials/" + name + "/Shader/fragment.glsl");

}

void Material::initMaterial() {

	//------------------- VERTEX SHADER
	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);

	const char *vertex = vertexCode.c_str();

	glShaderSource(vertexShader, 1, &vertex, NULL);
	glCompileShader(vertexShader);

	checkIfShaderCompile(vertexShader, "vertex");

	//-------------------- FRAGMENT SHADER
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	const char *fragment = fragmentCode.c_str();
	
	glShaderSource(fragmentShader, 1, &fragment, NULL);
	glCompileShader(fragmentShader);

	checkIfShaderCompile(fragmentShader, "fragment");

	//------------------- SHADER PROGRAM
	shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	checkIfProgramLinked(shaderProgram);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

}

void Material::update(float time) {

}

void Material::use() {

	glUseProgram(shaderProgram);

}

void Material::setBool(std::string name, bool value) {

	glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), (int)value);

}

void Material::setInt(std::string name, int value) {

	glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), value);

}

void Material::setFloat(std::string name, float value) {

	glUniform1f(glGetUniformLocation(shaderProgram, name.c_str()), value);

}

void Material::setVec3(std::string name, glm::vec3 value) {
	
	glUniform3f(glGetUniformLocation(shaderProgram, name.c_str()), value.x, value.y, value.z);

}

void Material::setVec4(std::string name, glm::vec4 value) {

	glUniform4f(glGetUniformLocation(shaderProgram, name.c_str()), value.x, value.y, value.z, value.w);

}

void Material::setMat4(std::string name, glm::mat4 value) {

	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));

}

std::string Material::getMaterialName() {
	return name;
}

void Material::checkIfShaderCompile(unsigned int shader, std::string type) {

	int success;
	char infoLog[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	if (!success) {
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		std::cout << "Erreur lors de la compilation d'un shader de " << this->name << " " << type << "   : \n" << infoLog << std::endl;
	}

}

void Material::checkIfProgramLinked(unsigned int program) {

	int success;
	char infoLog[1024];
	glGetProgramiv(program, GL_LINK_STATUS, &success);

	if (!success) {
		glGetProgramInfoLog(program, 1024, NULL, infoLog);
		std::cout << "Erreur lors du link du shader program de " << this->name << "  : \n" << infoLog << std::endl;
	}

}