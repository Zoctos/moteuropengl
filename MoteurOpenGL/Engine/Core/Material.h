#pragma once

#include <iostream>
#include <string>
#include <vector>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "../Tools/Tools.h"

class Material {

public:
	Material(std::string name);

	void initMaterial();

	virtual void update(float time);

	void use();

	void setBool(std::string name, bool value);
	void setInt(std::string name, int value);
	void setFloat(std::string name, float value);
	void setVec3(std::string name, glm::vec3 value);
	void setVec4(std::string name, glm::vec4 value);
	void setMat4(std::string name, glm::mat4 value);

	std::string getMaterialName();

private:

	void checkIfShaderCompile(unsigned int shader, std::string type);
	void checkIfProgramLinked(unsigned int program);

	std::string name;
	std::string vertexCode;
	std::string fragmentCode;

	unsigned int shaderProgram;

};