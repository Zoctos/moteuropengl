#include "Mesh.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

Mesh::Mesh(std::string materialName) {

	pos = glm::vec3(0.0f, 0.0f, 0.0f);
	model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	this->materialName = materialName;
	size = glm::vec3(1.0f);

	material = new Material(materialName);

	isMesh = false;
	form = "cube";

	rotate = glm::vec3(1.0f);
	angle = 0.0f;

}

Mesh::Mesh(Material *material) {

	pos = glm::vec3(0.0f, 0.0f, 0.0f);
	model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	this->materialName = material->getMaterialName();
	size = glm::vec3(1.0f);

	this->material = material;

	isMesh = false;
	form = "cube";

	rotate = glm::vec3(1.0f);
	angle = 0.0f;

	haveNormal = false;
}

Mesh::Mesh(Material *material, std::string mesh) {

	pos = glm::vec3(0.0f, 0.0f, 0.0f);
	model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	this->materialName = material->getMaterialName();
	size = glm::vec3(1.0f);

	this->material = material;

	isMesh = true;

	std::string path = "Models/" + mesh;

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &err, path.c_str())) {
		throw std::runtime_error(err);
	}

	std::unordered_map<Vertex, uint32_t> uniqueVertices = {};

	haveNormal = false;

	if (attrib.normals.size() > 0) {
		haveNormal = true;
	}

	for (const auto &shape : shapes) {
		for (const auto &index : shape.mesh.indices) {

			Vertex v = Vertex();
			v.position = glm::vec3(attrib.vertices[3 * index.vertex_index + 0], attrib.vertices[3 * index.vertex_index + 1], attrib.vertices[3 * index.vertex_index + 2]);
			v.texCoords = glm::vec2(attrib.texcoords[2 * index.texcoord_index + 0], 1.0f - attrib.texcoords[2 * index.texcoord_index + 1]);
			
			if (haveNormal) {
				v.normal = glm::vec3(attrib.normals[3 * index.vertex_index + 0], attrib.normals[3 * index.vertex_index + 1], attrib.normals[3 * index.vertex_index + 2]);
			}


			if (uniqueVertices.count(v) == 0) {
				uniqueVertices[v] = static_cast<uint32_t>(vertices.size());
				vertices.push_back(v);
			}

			indices.push_back(uniqueVertices[v]);

		}
	}

	rotate = glm::vec3(1.0f);
	angle = 0.0f;

}

void Mesh::initMesh() {

	if (!isMesh) {
		
		if (form == "cube") {
			setCube();
		}
		else if (form == "plane") {
			setPlane();
		}
	}

	glGenVertexArrays(1, &VAO);

	//Cr�ation du Vertex Buffer Object
	glGenBuffers(1, &VBO);

	//Cr�ation du Element Buffer Object
	glGenBuffers(1, &EBO);

	//Cr�ation du Vertex Array Object
	glBindVertexArray(VAO);

	//On affecte les vertices au VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(), GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*vertices.size(), &vertices[0], GL_STATIC_DRAW);

	//On affecte les indices au EBO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices.data(), GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*indices.size(), &indices[0], GL_STATIC_DRAW);

	//position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

	//texture
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(offsetof(Vertex, texCoords)));

	//normal
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(offsetof(Vertex, normal)));

	//Initialisation du mat�riau
	material->initMaterial();

}

void Mesh::translate(glm::vec3 t) {

	pos += t;

}

void Mesh::scale(glm::vec3 s) {

	size = s;

}

void Mesh::rotation(glm::vec3 r, float angle) {
	this->rotate = r;
	this->angle = angle;
}

void Mesh::bindTextures() {

	/*
	for (int i = 0; i < textures.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		textures[i]->bindTexture();
	}
	*/
	if (textures.size() > 0) {
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		material->setInt("objectTexture", 0);
		textures[0]->bindTexture();
	}
	
}

void Mesh::prepareMaterial(float time) {

	material->setMat4("model", getModel());

	if (textures.size() == 0) {
		material->setBool("haveTexture", false);
	}
	else {
		material->setBool("haveTexture", true);
	}

	material->update(time);

}

void Mesh::draw(float time) {

	glBindVertexArray(VAO);

	if (isMesh || indices.size() > 0) {
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	}
	else {
		if (form == "cube") {
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}
		
	}

	glBindVertexArray(0);

}

void Mesh::addTexture(std::string name) {

	Texture *t = Tools::loadTexture(name);
	textures.push_back(t);

}

void Mesh::setPos(glm::vec3 pos) {

	this->pos = pos;

}

void Mesh::setForm(std::string form) {
	this->form = form;
}

Material* Mesh::getMaterial() {
	return this->material;
}

glm::vec3 Mesh::getPos() {
	return this->pos;
}

glm::vec3 Mesh::getSize() {
	return this->size;
}

glm::mat4 Mesh::getModel() {
	glm::mat4 model;

	model = glm::translate(model, getPos());
	model = glm::scale(model, this->size);

	model = glm::rotate(model, glm::radians(angle), this->rotate);
	return model;
}

void Mesh::setPlane() {

	Vertex v1 = Vertex(glm::vec3(1.0f, 1.0f, 0.0f), glm::vec3(0.0f), glm::vec2(1.0f, 1.0f));
	Vertex v2 = Vertex(glm::vec3(1.0f, -1.0f, 0.0f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f));
	Vertex v3 = Vertex(glm::vec3(-1.0f, -1.0f, 0.0f), glm::vec3(0.0f), glm::vec2(0.0f, 0.0f));
	Vertex v4 = Vertex(glm::vec3(-1.0f, 1.0f, 0.0f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f));

	vertices = {
		v1, v2, v3, v4
	};

	indices = {
		0, 1, 3,
		1, 2, 3
	};

}

void Mesh::setCube() {

	vertices = {
		Vertex(glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 0.0f)),
		Vertex(glm::vec3(0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),
		Vertex(glm::vec3(0.5f, 0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 1.0f)),
		Vertex(glm::vec3(0.5f, 0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 1.0f)),
		Vertex(glm::vec3(-0.5f, 0.5f, -0.5f),glm::vec3(0.0f), glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 0.0f)),

		Vertex(glm::vec3(-0.5f, -0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 0.0f)),
		Vertex(glm::vec3(0.5f, -0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),
		Vertex(glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 1.0f)),
		Vertex(glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 1.0f)),
		Vertex(glm::vec3(-0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(-0.5f, -0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 0.0f)),

		Vertex(glm::vec3(-0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),
		Vertex(glm::vec3(-0.5f, 0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 1.0f)),
		Vertex(glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(-0.5f, -0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 0.0f)),
		Vertex(glm::vec3(-0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),

		Vertex(glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),
		Vertex(glm::vec3(0.5f, 0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 1.0f)),
		Vertex(glm::vec3(0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(0.5f, -0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 0.0f)),
		Vertex(glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),

		Vertex(glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 1.0f)),
		Vertex(glm::vec3(0.5f, -0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),
		Vertex(glm::vec3(0.5f, -0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),
		Vertex(glm::vec3(-0.5f, -0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 0.0f)),
		Vertex(glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f)),

		Vertex(glm::vec3(-0.5f, 0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f)),
		Vertex(glm::vec3(0.5f, 0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 1.0f)),
		Vertex(glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),
		Vertex(glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(1.0f, 0.0f)),
		Vertex(glm::vec3(-0.5f, 0.5f, 0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 0.0f)),
		Vertex(glm::vec3(-0.5f, 0.5f, -0.5f), glm::vec3(0.0f), glm::vec2(0.0f, 1.0f))
	};

}