#pragma once

#include <glm\gtx\hash.hpp>

#include <unordered_map>

#include "Material.h"

struct Vertex {

public:
	glm::vec3 position;
	glm::vec2 texCoords;
	glm::vec3 normal;

	Vertex() {
		
	}

	Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 texCoords) {
		this->position = position;
		this->normal = normal;
		this->texCoords = texCoords;
	}

	bool operator==(const Vertex &other) const {
		return position == other.position && texCoords == other.texCoords && normal == other.normal;
	};
};

class Mesh {

public:

	Mesh(std::string materialName);
	Mesh(Material *m);
	Mesh(Material *m, std::string mesh);

	void initMesh();

	void translate(glm::vec3 t);
	void scale(glm::vec3 s);
	void rotation(glm::vec3 r, float angle);

	void bindTextures();

	void prepareMaterial(float time);
	void draw(float time);

	void addTexture(std::string name);

	void setPos(glm::vec3 pos);
	void setForm(std::string form);

	Material *getMaterial();
	glm::vec3 getPos();
	glm::vec3 getSize();

	glm::mat4 getModel();

private:
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	std::string materialName;

	Material *material;
	std::vector<Texture*> textures;

	unsigned int VBO; //vertices
	unsigned int EBO; //indices
	unsigned int VAO; //

	glm::vec3 pos;
	glm::vec3 size;
	glm::vec3 rotate;
	float angle;

	bool haveNormal;

	glm::mat4 model;

	glm::vec3 color;

	bool isMesh;
	std::string form;

	void setPlane();
	void setCube();

};

namespace std {
	template<> struct hash<Vertex> {
		size_t operator()(Vertex const &v) const {
			return ((hash<glm::vec3>()(v.position) ^
				(hash<glm::vec2>()(v.texCoords) << 1)) >> 1) ^
				(hash<glm::vec3>()(v.normal) << 1);
		}
	};
}