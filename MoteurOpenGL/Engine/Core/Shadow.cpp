#include "Shadow.h"

Shadow::Shadow(int width, int height) : Framebuffer(width, height, "Shadow") {

	//framebuffer
	glGenFramebuffers(1, &fbo);

	//depth texture
	texture = new DepthTexture(width, height);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture->getID(), 0);

	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	//check the status of the framebuffer
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "error when creating shadow" << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//create the material and the quad needed to render after
	material = new Material("Shadow");
	material->initMaterial();

	quad = new Mesh(new DepthMaterial());
	quad->setForm("plane");
	quad->initMesh();

}

void Shadow::configureLight(Light *light, Camera *camera) {

	float near_plane = 1.0f, far_plane = 7.5f;

	glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
	//glm::mat4 lightProjection = glm::perspective(45.0f, 1.0f, 2.0f, 50.0f);
	//glm::mat4 lightProjection = glm::perspective(glm::radians(45.0f), 1.0f, near_plane, far_plane);

	//glm::mat4 lightView = glm::lookAt(light->pos, camera->getPos(), glm::vec3(0.0f, 1.0f, 0.0f));
	//glm::mat4 lightView = glm::lookAt(glm::vec3(-2.0f, 4.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//glm::mat4 lightView = glm::lookAt(light->pos, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 lightView = glm::lookAt(light->pos, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	lightSpaceMatrix = lightProjection * lightView;

	material->use();
	material->setMat4("lightSpaceMatrix", lightSpaceMatrix);

}

void Shadow::bindTexture() {
	glBindTexture(GL_TEXTURE_2D, texture->getID());
}

void Shadow::render(float time) {

	quad->getMaterial()->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->getID());
	quad->getMaterial()->setInt("screen", 0);
	quad->getMaterial()->setBool("isOrthographic", true);
	quad->getMaterial()->setFloat("near", 1.0f);
	quad->getMaterial()->setFloat("far", 7.5f);

	quad->draw(time);

}

glm::mat4 Shadow::getLightSpaceMatrix() {
	return lightSpaceMatrix;
}