#pragma once

#include "Camera.h"
#include "DepthTexture.h"
#include "Framebuffer.h"
#include "Light.h"

#include "../../Materials/DepthMaterial/DepthMaterial.h"

#include <iostream>

class Shadow : public Framebuffer{

public:

	Shadow(int width, int height);

	void configureLight(Light *light, Camera *camera);

	void render(float time);

	void bindTexture();

	glm::mat4 getLightSpaceMatrix();

	Material * material;

private:

	glm::mat4 lightSpaceMatrix;

};