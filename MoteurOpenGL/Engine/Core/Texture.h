#pragma once

#include <GL\glew.h>
#include <GLFW\glfw3.h>

class Texture {

public:
	Texture(int width, int height);
	Texture(unsigned char *data, int width, int height, int nrChannels);

	void bindTexture();

	int getID();

protected:
	int width;
	int height;
	int nrChannels;

	unsigned int ID;

};