#include "Tools.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <fstream>
#include <sstream>

std::string Tools::readFile(const std::string &filename) {

	//ate : on commence a lire a la fin du fichier
	//std::ifstream file(filename, std::ios::ate | std::ios::binary);
	std::ifstream file;

	file.open(filename);

	if (!file.is_open()) {
		throw std::runtime_error("failed to open file !");
	}

	std::stringstream fileStream;

	fileStream << file.rdbuf();

	file.close();

	return fileStream.str();

}

Texture* Tools::loadTexture(std::string filename) {

	int width, height, nrChannels;

	unsigned char *data = stbi_load(("Textures/"+filename).c_str(), &width, &height, &nrChannels, 0);

	Texture *tex = new Texture(data, width, height, nrChannels);

	stbi_image_free(data);

	return tex;
}