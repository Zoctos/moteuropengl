#pragma once

#include <vector>

#include "../Core/Texture.h"

class Tools {

public:

	static std::string readFile(const std::string &filename);

	static Texture* loadTexture(std::string filename);

};