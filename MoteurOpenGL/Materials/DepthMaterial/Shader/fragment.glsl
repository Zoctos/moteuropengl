#version 330 core

uniform sampler2D screen;
uniform bool isOrthographic;
uniform float near;
uniform float far;

in vec2 mTexCoord;

out vec4 FragColor;

float LinearizeDepth(float depth){
	float z = depth * 2.0f - 1.0f;
	return (2.0f * near * far) / (far + near - z * (far - near));
}

void main(){

	float depth = 0.0f;
	
	if (isOrthographic) {
		depth = texture(screen, mTexCoord).r;
	}
	else {
		depth = LinearizeDepth(gl_FragCoord.z) / far;
	}

	FragColor = vec4(vec3(depth), 1.0f);

}