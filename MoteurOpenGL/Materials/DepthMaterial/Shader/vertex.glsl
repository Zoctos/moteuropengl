#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 texCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec2 mTexCoord;

out gl_PerVertex {
    vec4 gl_Position;
};

void main(){

	//gl_Position = proj * view * model * vec4(pos, 1.0f);
	gl_Position = vec4(pos, 1.0f);
	mTexCoord = texCoord;

}