#version 330 core

uniform sampler2D screen;

in vec2 mTexCoord;

out vec4 FragColor;

void main(){

	vec3 col = texture(screen, mTexCoord).rgb;
	FragColor = vec4(col, 1.0f);

}