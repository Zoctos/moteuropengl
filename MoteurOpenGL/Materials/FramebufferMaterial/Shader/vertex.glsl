#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 texCoord;

out vec2 mTexCoord;

out gl_PerVertex {
    vec4 gl_Position;
};

void main(){

	gl_Position = vec4(pos.x, pos.y, 0.0f, 1.0f);
	mTexCoord = texCoord;

}