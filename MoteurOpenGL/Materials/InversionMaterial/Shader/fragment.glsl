#version 330 core

uniform sampler2D screen;

in vec2 mTexCoord;

out vec4 FragColor;

void main(){

	FragColor = vec4(vec3(1.0f - texture(screen, mTexCoord)), 1.0f);

}