#include "PhongMaterial.h"

#include <iostream>

PhongMaterial::PhongMaterial() : Material("PhongMaterial") {

	this->color = glm::vec3(0.0f, 0.0f, 0.0f);

}

void PhongMaterial::update(float time) {

	setVec3("color", color);

}

void PhongMaterial::setColor(glm::vec3 c) {
	this->color = c;
}