#pragma once

#include "../../Engine/Core/Material.h"

class PhongMaterial : public Material {

public:
	PhongMaterial();

	void update(float time);
	void setColor(glm::vec3 c);

private:

	glm::vec3 color;

};