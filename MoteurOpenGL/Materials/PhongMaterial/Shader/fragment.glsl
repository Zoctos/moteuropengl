#version 330 core

uniform sampler2D objectTexture;
uniform sampler2D shadowMap;

uniform vec3 color;
uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 camera;
uniform bool haveTexture;

in vec2 mTexCoord;
in vec3 mPos;
in vec3 mNormal;
in vec4 shadowCoord;

out vec4 FragColor;

//Method if we want to have "soft" shadows
float shadowComputeMultiple(vec4 posLightSpace, vec3 normal, vec3 lightDir) {

	vec3 projCoords = posLightSpace.xyz / posLightSpace.w;
	projCoords = projCoords * 0.5f + 0.5f;

	float currentDepth = projCoords.z;

	float shadow = 0.0f;
	float bias = max(0.05f * (1.0f - dot(normal, lightDir)), 0.005f);

	//we get the size of one texel
	vec2 texelSize = 1.0f / textureSize(shadowMap, 0);

	int nbMinMax = 1;

	for (int x = -nbMinMax; x <= nbMinMax; x++) {
		for (int y = -nbMinMax; y <= nbMinMax; y++) {
			float pcf = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
			if (currentDepth - bias >= pcf) {
				shadow += 0.5f;
			}
		}
	}

	shadow /= 9.0f;

	if (projCoords.z > 1.0f)
		shadow = 0.0f;

	return shadow;

}

//Methods if we want "hard shadows"
float shadowCompute(vec4 posLightSpace, vec3 normal, vec3 lightDir) {

	vec3 projCoords = posLightSpace.xyz / posLightSpace.w;
	projCoords = projCoords * 0.5f + 0.5f;

	float closestDepth = texture(shadowMap, projCoords.xy).r;

	float currentDepth = projCoords.z;

	float shadow = 0.0f;
	float bias = max(0.05f * (1.0f - dot(normal, lightDir)), 0.005f);

	if (currentDepth - bias >= closestDepth) {
		shadow = 0.5f;
	}

	if (projCoords.z > 1.0f)
		shadow = 0.0f;

	return shadow;

}

void main(){

	vec3 ambient = vec3(0.0f);

	//get the ambient color
	if (haveTexture) {
		ambient = texture(objectTexture, mTexCoord).rgb;
	}
	else {
		ambient = color;
	}

	ambient *= 0.15f;

	//compute diffuse value
	vec3 vLight = normalize(lightPos - mPos);
	float tetaLight = dot(vLight, mNormal);
	vec3 diffus = lightColor * max(tetaLight, 0.0f);

	//compute specular value
	vec3 vOrigine = normalize(camera - mPos);

	vec3 vReflect = normalize(reflect(vLight, mNormal));
	float tetaVue = dot(vReflect, vOrigine);

	vec3 specular = vec3(1.0) * pow(max(tetaVue, 0.0f), 256);

	//get the shadow value
	//float shadow = shadowCompute(shadowCoord, mNormal, vLight);
	float shadow = shadowComputeMultiple(shadowCoord, mNormal, vLight);

	//compute the final color
	vec3 calcColor = ambient + (1.0f - shadow) * (diffus * 0.5f + specular * 0.5f);

	FragColor = vec4(calcColor, 1.0f);

}