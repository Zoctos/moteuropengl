#version 330 core

uniform sampler2D objectTexture;
uniform sampler2D shadowMap;

uniform vec3 lightPos;

in vec3 mPos;
in vec2 mTexCoord;
in vec3 mNormal;
in vec4 shadowCoord;

out vec4 FragColor;

//Methods if we want "hard shadows"
float shadowCompute(vec4 posLightSpace, vec3 normal, vec3 lightDir) {

	vec3 projCoords = posLightSpace.xyz / posLightSpace.w;
	projCoords = projCoords * 0.5f + 0.5f;

	float closestDepth = texture(shadowMap, projCoords.xy).r;

	float currentDepth = projCoords.z;

	float shadow = 0.0f;
	float bias = max(0.05f * (1.0f - dot(normal, lightDir)), 0.005f);

	if (currentDepth - bias >= closestDepth) {
		shadow = 0.5f;
	}

	if (projCoords.z > 1.0f)
		shadow = 0.0f;

	return shadow;

}

void main(){

	vec3 vLight = normalize(lightPos - mPos);

	vec4 texColor = texture(objectTexture, mTexCoord);

	if(texColor.a < 0.5f){
		texColor.r = 1.0f;
	}

	//get the shadow value
	float shadow = shadowCompute(shadowCoord, mNormal, vLight);

	vec3 ambientShadow = vec3(0.5f * shadow);

	FragColor = vec4(texColor.xyz - ambientShadow, 1.0f);

}