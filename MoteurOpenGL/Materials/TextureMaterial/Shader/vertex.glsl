#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec3 normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform mat4 lightSpaceMatrix;

out vec3 mPos;
out vec2 mTexCoord;
out vec3 mNormal;
out vec4 shadowCoord;

out gl_PerVertex {
    vec4 gl_Position;
};

void main(){

	mPos = vec3(model * vec4(pos, 1.0f));
	mNormal = normal;

	gl_Position = proj * view * model * vec4(pos, 1.0f);
	//gl_Position = vec4(pos, 1.0f);
	
	mTexCoord = texCoord;
	//vec3 fragPos = vec3(model * vec4(pos, 1.0f));
	shadowCoord = lightSpaceMatrix * model * vec4(pos, 1.0f);

}