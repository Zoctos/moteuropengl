#version 330

uniform sampler2D texture;

uniform vec3 color;
uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 camera;

in vec3 vNormale;
in vec3 myPos;
in vec3 texCoords;

out vec4 FragColor;

void main()
{
	
	vec4 col = vec4(color, 1.0f);
	col = texture(texture, texCoords.xy);

	vec3 vLight = normalize(myPos - lightPos);
	float tetaLight = dot(-vLight, vNormale);

	vec3 vOrigine = normalize(camera - myPos);

	vec3 vReflect = normalize(reflect(vLight, vNormale));
	float tetaVue = dot(vReflect, vOrigine);

	vec3 ambiant = col.xyz;
	
	vec3 diffuse = col.xyz * max(tetaLight, 0.0f);
	
	vec3 specular = vec3(pow(max(tetaVue, 0.0f), 64));

	vec3 colorLight = vec3(0.25*ambiant + 0.25f*diffuse + 0.5f*specular);

	FragColor = vec4(colorLight, 0.5);

}