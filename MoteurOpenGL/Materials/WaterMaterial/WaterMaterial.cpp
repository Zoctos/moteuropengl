#include "WaterMaterial.h"

WaterMaterial::WaterMaterial() : Material("WaterMaterial") {

	positionWave = glm::vec3(0.0f);
	pointPosition = glm::vec3(0.0f);
	isWaveMove = false;
	methodWave = true;
	speed = 1.0f;

}

void WaterMaterial::update(float time) {

	setVec3("positionWave", positionWave);
	setVec3("pointPosition", pointPosition);
	setBool("isWaveMove", isWaveMove);
	setBool("methodWave", methodWave);
	setFloat("speed", speed);
	setFloat("time", time);

}

void WaterMaterial::setPositionWave(glm::vec3 positionWave) {
	this->positionWave = positionWave;
}

void WaterMaterial::setPointPosition(glm::vec3 pointPosition) {
	this->pointPosition = pointPosition;
}

void WaterMaterial::setIsWaveMove(bool isWaveMove) {
	this->isWaveMove = isWaveMove;
}

void WaterMaterial::setMethodWave(bool methodWave) {
	this->methodWave = methodWave;
}

void WaterMaterial::setSpeed(float speed) {
	this->speed = speed;
}