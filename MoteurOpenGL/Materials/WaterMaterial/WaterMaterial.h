#pragma once

#include "../../Engine/Core/Material.h"

class WaterMaterial : public Material {

public:
	WaterMaterial();

	void update(float time);

	void setPositionWave(glm::vec3 positionWave);
	void setPointPosition(glm::vec3 pointPosition);
	void setIsWaveMove(bool isWaveMove);
	void setMethodWave(bool methodWave);
	void setSpeed(float speed);

private:

	glm::vec3 positionWave;
	glm::vec3 pointPosition;
	float time;
	bool isWaveMove;
	bool methodWave;
	float speed;

};