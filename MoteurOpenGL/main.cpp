
#include "Engine\Core\App.h"

int main() {

	App* app = new App();

	app->init();

	app->run();

	return 0;
}